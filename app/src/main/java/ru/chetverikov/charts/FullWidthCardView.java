package ru.chetverikov.charts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import ru.chetverikov.charts.util.PixelUtil;

public class FullWidthCardView extends LinearLayout {

	private final static float SHADOW_MULTIPLIER = 1.5f;
	private final static int SHADOW_START_COLOR = 0x07000000;
	private final static int SHADOW_END_COLOR = 0x03000000;

	private final Paint paint;
	private final float shadowSize;

	private Paint paintBackground;

	public FullWidthCardView(Context context) {
		this(context, null);
	}

	public FullWidthCardView(Context context, @Nullable AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public FullWidthCardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		float elevation = PixelUtil.dp(getResources(), 3);
		paintBackground = new Paint();
		paintBackground.setStyle(Paint.Style.FILL);
		paintBackground.setColor(ContextCompat.getColor(context, R.color.background));

		float rawShadowSize = calculateShadowSize(elevation);
		paint = new Paint(Paint.DITHER_FLAG);
		paint.setStyle(Paint.Style.FILL);
		paint.setDither(true);
		paint.setShader(
			new LinearGradient(
				0, 0, 0, rawShadowSize,
				new int[] { SHADOW_START_COLOR, SHADOW_START_COLOR, SHADOW_END_COLOR },
				new float[] { 0f, .2f, 1f }, Shader.TileMode.CLAMP
			)
		);
		paint.setAntiAlias(false);

		shadowSize = rawShadowSize;//- PixelUtil.dp(getResources(), 1);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int desiredWidth = getMeasuredWidth();
		int desiredHeight = getMeasuredHeight() + (int) shadowSize;

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width;
		int height;
		if (widthMode == MeasureSpec.EXACTLY) {
			width = widthSize;
		} else if (widthMode == MeasureSpec.AT_MOST) {
			width = Math.min(desiredWidth, widthSize);
		} else {
			width = desiredWidth;
		}
		if (heightMode == MeasureSpec.EXACTLY) {
			height = heightSize;
		} else if (heightMode == MeasureSpec.AT_MOST) {
			height = Math.min(desiredHeight, heightSize);
		} else {
			height = desiredHeight;
		}
		setMeasuredDimension(width, height);
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		canvas.drawRect(
			0, shadowSize / 3f, getMeasuredWidth(), getMeasuredHeight() - shadowSize,
			paintBackground
		);
		drawShadow(
			canvas, getMeasuredWidth(), getMeasuredHeight() - shadowSize
		);
		super.dispatchDraw(canvas);
	}

	@Override
	public void setBackgroundColor(int color) {
		paintBackground.setColor(color);
		invalidate();
	}

	private void drawShadow(Canvas canvas, float width, float yStart) {
		int saved = canvas.save();
		canvas.translate(0, yStart);
		canvas.drawRect(0, 0, width, shadowSize, paint);
		canvas.restoreToCount(saved);
	}

	private float calculateShadowSize(float elevation) {
		return (int) (elevation * SHADOW_MULTIPLIER);
	}
}