package ru.chetverikov.charts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.core.widget.CompoundButtonCompat;
import ru.chetverikov.charts.util.PixelUtil;

public class WhiteHorseCheckBoxView extends AppCompatCheckBox {

	@Nullable
	private Paint paint;
	private float dp8;

	public WhiteHorseCheckBoxView(Context context) {
		super(context);
	}

	public WhiteHorseCheckBoxView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public WhiteHorseCheckBoxView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (isChecked()) {
			if (paint == null) {
				paint = new Paint(Paint.ANTI_ALIAS_FLAG);
				paint.setStyle(Paint.Style.FILL);
				paint.setColor(ContextCompat.getColor(getContext(), R.color.white));

				dp8 = PixelUtil.dp(getResources(), 8);
			}
			Drawable drawable = CompoundButtonCompat.getButtonDrawable(this);
			if (drawable != null) {
				canvas.drawRect(
					dp8,
					getHeight() - drawable.getIntrinsicHeight(),
					drawable.getIntrinsicWidth() - dp8,
					drawable.getIntrinsicHeight(),
					paint
				);
			}
		}
		super.onDraw(canvas);
	}
}