package ru.chetverikov.charts;

import android.app.Application;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatDelegate;

public class App extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		NightModeManager nightModeManager = new NightModeManager(
			PreferenceManager.getDefaultSharedPreferences(this)
		);
		if (nightModeManager.isNightModeSet()) {
			AppCompatDelegate.setDefaultNightMode(
				nightModeManager.isNightModeEnabled()
					? AppCompatDelegate.MODE_NIGHT_YES
					: AppCompatDelegate.MODE_NIGHT_NO
			);
		}
	}
}