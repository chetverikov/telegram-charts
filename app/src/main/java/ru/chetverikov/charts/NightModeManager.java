package ru.chetverikov.charts;

import android.content.SharedPreferences;

import androidx.annotation.NonNull;

public class NightModeManager {

	private static final String PREFS_IS_NIGHT_MODE_ENABLED = "is_night_mode_enabled";

	@NonNull
	private final SharedPreferences prefs;

	public NightModeManager(@NonNull SharedPreferences prefs) {
		this.prefs = prefs;
	}

	public boolean isNightModeSet() {
		return prefs.contains(PREFS_IS_NIGHT_MODE_ENABLED);
	}

	public boolean isNightModeEnabled() {
		return prefs.getBoolean(PREFS_IS_NIGHT_MODE_ENABLED, false);
	}

	public void setNightModeEnabled(boolean value) {
		prefs.edit().putBoolean(PREFS_IS_NIGHT_MODE_ENABLED, value).apply();
	}
}