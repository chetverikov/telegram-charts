package ru.chetverikov.charts.util;

import android.content.res.Resources;
import android.util.TypedValue;

public final class PixelUtil {

	public static float sp(Resources res, float sp) {
		return TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_SP,
			sp,
			res.getDisplayMetrics()
		);
	}

	public static float dp(Resources res, float dp) {
		return TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP,
			dp,
			res.getDisplayMetrics()
		);
	}
}