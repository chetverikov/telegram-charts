package ru.chetverikov.charts;

import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.core.util.ObjectsCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.CompoundButtonCompat;
import androidx.core.widget.NestedScrollView;
import ru.chetverikov.charts.model.Chart;
import ru.chetverikov.charts.model.ChartDataParser;
import ru.chetverikov.charts.model.Line;
import ru.chetverikov.charts.util.PixelUtil;

public class MainActivity extends AppCompatActivity {

	private final static String TAG = "Charts";

	private NightModeManager nightModeManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		nightModeManager = new NightModeManager(
			PreferenceManager.getDefaultSharedPreferences(this)
		);

		final InputStream in = getResources().openRawResource(R.raw.chart_data);
		final String json = readChangelogFile(in);

		Chart[] charts = null;
		try {
			charts = new ChartDataParser().parse(json);
		} catch (JSONException e) {
			Log.e(TAG, e.toString(), e);
		}
		if (charts == null || charts.length == 0) {
			Toast.makeText(this, "Chart parsing error!", Toast.LENGTH_SHORT).show();
			return;
		}

		int dp16 = (int) PixelUtil.dp(getResources(), 16);
		int dp1 = (int) PixelUtil.dp(getResources(), 1);

		NestedScrollView nestedScrollView = new NestedScrollView(this);
		nestedScrollView.setId(ObjectsCompat.hash("NestedScrollView"));
		ViewCompat.setScrollIndicators(nestedScrollView, ViewCompat.SCROLL_INDICATOR_RIGHT);

		LinearLayout linearLayout = new LinearLayout(this);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		nestedScrollView.addView(
			linearLayout,
			ViewGroup.LayoutParams.MATCH_PARENT,
			ViewGroup.LayoutParams.WRAP_CONTENT
		);

		for (int j = 0; j < charts.length; j++) {
			FullWidthCardView cardView = new FullWidthCardView(this);
			cardView.setOrientation(LinearLayout.VERTICAL);
			ViewGroup.MarginLayoutParams cardViewParams = new ViewGroup.MarginLayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
			);
			cardViewParams.bottomMargin = dp16 * 2;
			linearLayout.addView(cardView, cardViewParams);

			Chart chart = charts[j];
			chart.name = "Followers #" + (j + 1);
			final ChartView chartView = new ChartView(this);
			chartView.setId(ObjectsCompat.hash("ChartView", chart.name));
			chartView.setChart(chart);
			cardView.addView(
				chartView,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT
			);

			for (int i = 0; i < chart.lines.length; i++) {
				final Line line = chart.lines[i];
				if (i > 0) {
					View divider = new View(this);
					divider.setBackgroundResource(R.color.chartControlDivider);
					ViewGroup.MarginLayoutParams dividerParams = new ViewGroup.MarginLayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT, dp1
					);
					dividerParams.leftMargin = dp16 * 4;
					cardView.addView(divider, dividerParams);
				}

				CheckBox lineControl = new WhiteHorseCheckBoxView(this);
				lineControl.setId(ObjectsCompat.hash("CheckBox", chart.name, line.id));
				lineControl.setText(line.name);
				lineControl.setTextColor(ContextCompat.getColor(this, R.color.text));
				lineControl.setTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL));
				CompoundButtonCompat.setButtonTintList(lineControl, ColorStateList.valueOf(line.color));
				lineControl.setChecked(true);
				lineControl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						chartView.setLineEnabled(line.id, isChecked);
					}
				});
				lineControl.setPadding(dp16, dp16, dp16, dp16);
				ViewGroup.MarginLayoutParams lineControlParams = new ViewGroup.MarginLayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
				);
				lineControlParams.leftMargin = dp16;
				lineControlParams.rightMargin = dp16;
				cardView.addView(lineControl, lineControlParams);
			}
		}

		setContentView(nestedScrollView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_go_night) {
			int nightMode = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
			if (nightMode == Configuration.UI_MODE_NIGHT_YES) {
				AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
				nightModeManager.setNightModeEnabled(false);
			} else {
				AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
				nightModeManager.setNightModeEnabled(true);
			}
			getDelegate().applyDayNight();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@NonNull
	private String readChangelogFile(InputStream inputStream) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		byte buf[] = new byte[1024];
		int len;
		try {
			while ((len = inputStream.read(buf)) != -1) {
				outputStream.write(buf, 0, len);
			}
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		} finally {
			try {
				outputStream.close();
				inputStream.close();
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
			}
		}
		return outputStream.toString();
	}
}