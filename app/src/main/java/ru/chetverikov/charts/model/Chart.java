package ru.chetverikov.charts.model;

import java.util.Arrays;

public class Chart {
	public String name;
	public long[] xValues;
	public Line[] lines;
	public long yMinValue;
	public long yMaxValue;

	@Override
	public String toString() {
		return "Chart{" +
			"name=" + name +
			", xValues=" + xValues.length +
			", lines=" + Arrays.toString(lines) +
			'}';
	}
}