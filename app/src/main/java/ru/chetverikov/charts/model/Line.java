package ru.chetverikov.charts.model;

public class Line {
	public final String id;
	public String name;
	public int color;
	public long[] values;
	public long minValue;
	public long maxValue;

	public Line(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Line{" +
			"id='" + id + '\'' +
			", name='" + name + '\'' +
			", color=" + color +
			", values=" + values.length +
			'}';
	}
}