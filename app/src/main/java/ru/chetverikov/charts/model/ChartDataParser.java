package ru.chetverikov.charts.model;

import android.graphics.Color;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ChartDataParser {

	@Nullable
	public Chart[] parse(@NonNull String jsonString) throws JSONException {
		JSONArray json = new JSONArray(jsonString);
		int chartsCount = json.length();
		if (chartsCount == 0) {
			return null;
		}
		Chart[] charts = new Chart[chartsCount];
		for (int i = 0; i < chartsCount; i++) {
			JSONObject object = json.getJSONObject(i);

			charts[i] = new Chart();
			charts[i].yMinValue = Long.MAX_VALUE;
			charts[i].yMaxValue = Long.MIN_VALUE;

			String xValuesId = null;
			Map<String, Line> lines = new LinkedHashMap<>();

			JSONObject types = object.getJSONObject("types");
			Iterator<String> typesKey = types.keys();
			while (typesKey.hasNext()) {
				String key = typesKey.next();
				String value = types.getString(key);
				if ("line".equals(value)) {
					lines.put(key, new Line(key));
				} else if ("x".equals(value)) {
					xValuesId = key;
				}
			}

			charts[i].lines = new Line[lines.size()];
			JSONObject names = object.getJSONObject("names");
			JSONObject colors = object.getJSONObject("colors");
			int k = 0;
			for (Line line : lines.values()) {
				line.name = names.getString(line.id);
				line.color = Color.parseColor(colors.getString(line.id));
				charts[i].lines[k] = line;
				k++;
			}

			JSONArray columns = object.getJSONArray("columns");
			for (int j = 0; j < columns.length(); j++) {
				JSONArray values = columns.getJSONArray(j);
				int valuesLen = values.length();
				if (valuesLen == 0) {
					continue;
				}
				String id = values.getString(0);
				if (id.equals(xValuesId)) {
					charts[i].xValues = new long[valuesLen - 1];
					for (int q = 1; q < valuesLen; q++) {
						charts[i].xValues[q - 1] = values.getLong(q);
					}
					continue;
				}
				Line line = lines.get(id);
				if (line == null) {
					continue;
				}
				line.minValue = Long.MAX_VALUE;
				line.maxValue = Long.MIN_VALUE;
				line.values = new long[valuesLen - 1];
				for (int q = 1; q < valuesLen; q++) {
					long value = values.getLong(q);
					if (value < line.minValue) {
						line.minValue = value;
					}
					if (value > line.maxValue) {
						line.maxValue = value;
					}
					if (value < charts[i].yMinValue) {
						charts[i].yMinValue = value;
					}
					if (value > charts[i].yMaxValue) {
						charts[i].yMaxValue = value;
					}
					line.values[q - 1] = value;
				}
			}
		}

		return charts;
	}
}