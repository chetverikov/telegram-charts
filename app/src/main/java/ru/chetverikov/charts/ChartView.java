package ru.chetverikov.charts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import ru.chetverikov.charts.model.Chart;
import ru.chetverikov.charts.model.Line;
import ru.chetverikov.charts.util.PixelUtil;

public class ChartView extends View {

	private static final String STATE_SUPER = "state_super";
	private static final String STATE_SELECTED_POINT = "state_selected_point";
	private static final String STATE_DISABLED_LINES = "state_disabled_lines";
	private static final String STATE_PREVIEW_LEFT_BORDER = "state_preview_left_border";
	private static final String STATE_PREVIEW_RIGHT_BORDER = "state_preview_right_border";

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MMM d", Locale.US);
	private static final SimpleDateFormat SELECTED_DATE_FORMAT = new SimpleDateFormat(
		"EEE, MMM d",
		Locale.US
	);
	private static final boolean DEBUG = false;

	private final float sp12;
	private final float sp16;
	private final float dp1;
	private final float dp2;
	private final float dp4;
	private final float dp8;
	private final float dp16;
	private final float dp32;

	private final Paint chartNamePaint;
	private final Paint chartLegendPaint;
	private final Paint verticalLinePaint;
	private final Paint horizontalLinePaint;
	private final Paint originLinePaint;
	private final Paint previewOutsidePaint;
	private final Paint previewFramePaint;
	private final Paint whiteFillPaint;
	private final Paint linePaint;
	private final Paint linePreviewPaint;
	private final Paint selectedBorderPaint;
	private final Paint selectedBackgroundPaint;
	private final Paint selectedDatePaint;
	private final Paint selectedLineValuePaint;
	private final Paint selectedLineNamePaint;
	private final Paint debugPaint;

	private final float dateMaxLen;

	@Nullable
	private Chart chart;
	@Nullable
	private Integer selectedPoint = null;
	private Set<String> disabledLines = new HashSet<>();

	private float previewMinSize;
	private float previewLeftBorderRatio = -1;
	private float previewRightBorderRatio = -1;
	private float previewLeftBorder = 0;
	private float previewRightBorder = 0;
	private float previousTouchEventX = 0;

	private int xSize;
	private float fullChartSize, fullChartShift;
	private long yMax = -1;

	private boolean isChartTouch = false;
	private boolean isPreviewTouch = false;
	private boolean isPreviewLeftBorderTouch = false;
	private boolean isPreviewRightBorderTouch = false;

	public ChartView(Context context) {
		this(context, null);
	}

	public ChartView(Context context, @Nullable AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		dp1 = PixelUtil.dp(getResources(), 1);
		dp2 = PixelUtil.dp(getResources(), 2);
		dp4 = PixelUtil.dp(getResources(), 4);
		dp8 = PixelUtil.dp(getResources(), 8);
		dp16 = PixelUtil.dp(getResources(), 16);
		dp32 = PixelUtil.dp(getResources(), 32);
		sp12 = PixelUtil.sp(getResources(), 12);
		sp16 = PixelUtil.sp(getResources(), 16);

		chartNamePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		chartNamePaint.setTextSize(sp16);
		chartNamePaint.setColor(ContextCompat.getColor(context, R.color.chartName));
		chartNamePaint.setTypeface(Typeface.DEFAULT_BOLD);

		chartLegendPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		chartLegendPaint.setTextSize(sp12);
		chartLegendPaint.setColor(ContextCompat.getColor(context, R.color.chartLegend));
		dateMaxLen = chartLegendPaint.measureText("FFF 77");

		selectedDatePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectedDatePaint.setTextSize(sp12);
		selectedDatePaint.setTypeface(Typeface.DEFAULT_BOLD);
		selectedDatePaint.setColor(ContextCompat.getColor(context, R.color.text));

		selectedLineValuePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectedLineValuePaint.setTextSize(sp16);
		selectedLineValuePaint.setTypeface(Typeface.DEFAULT_BOLD);
		selectedLineValuePaint.setColor(ContextCompat.getColor(context, R.color.red));

		selectedLineNamePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectedLineNamePaint.setTextSize(sp12);
		selectedLineNamePaint.setColor(ContextCompat.getColor(context, R.color.red));

		verticalLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		verticalLinePaint.setStyle(Paint.Style.STROKE);
		verticalLinePaint.setColor(ContextCompat.getColor(context, R.color.chartVerticalLine));
		verticalLinePaint.setStrokeWidth(dp1);

		horizontalLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		horizontalLinePaint.setStyle(Paint.Style.STROKE);
		horizontalLinePaint.setColor(ContextCompat.getColor(context, R.color.chartHorizontalLine));
		horizontalLinePaint.setStrokeWidth(dp1);

		originLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		originLinePaint.setStyle(Paint.Style.STROKE);
		originLinePaint.setColor(ContextCompat.getColor(context, R.color.chartOrigin));
		originLinePaint.setStrokeWidth(dp1);

		linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		linePaint.setStyle(Paint.Style.STROKE);
		linePaint.setColor(ContextCompat.getColor(context, R.color.chartOrigin));
		linePaint.setStrokeWidth(dp2);

		linePreviewPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		linePreviewPaint.setStyle(Paint.Style.STROKE);
		linePreviewPaint.setColor(ContextCompat.getColor(context, R.color.chartOrigin));
		linePreviewPaint.setStrokeWidth(dp1);

		selectedBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectedBorderPaint.setStyle(Paint.Style.STROKE);
		selectedBorderPaint.setColor(ContextCompat.getColor(context, R.color.chartSelectedBorder));
		selectedBorderPaint.setStrokeWidth(dp1);

		selectedBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectedBackgroundPaint.setStyle(Paint.Style.FILL);
		selectedBackgroundPaint.setColor(ContextCompat.getColor(context, R.color.chartSelectedBackground));

		previewOutsidePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		previewOutsidePaint.setStyle(Paint.Style.FILL);
		previewOutsidePaint.setColor(ContextCompat.getColor(context, R.color.chartPreviewOutside));

		previewFramePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		previewFramePaint.setStyle(Paint.Style.FILL);
		previewFramePaint.setColor(ContextCompat.getColor(context, R.color.chartPreviewFrame));

		whiteFillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		whiteFillPaint.setStyle(Paint.Style.FILL);
		whiteFillPaint.setColor(ContextCompat.getColor(context, R.color.background));

		debugPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		debugPaint.setStyle(Paint.Style.STROKE);
		debugPaint.setColor(ContextCompat.getColor(context, R.color.red));
		debugPaint.setStrokeWidth(1);

		setWillNotDraw(false);
		setSaveEnabled(true);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (chart == null) {
			return;
		}

		if (DEBUG) {
			canvas.save();
		}

		canvas.translate(dp16, dp16);
		canvas.drawText(chart.name, 0, sp16, chartNamePaint);

		canvas.translate(0, dp32);
		float chartHeight = dp16 * 16;
		float xChartLen = getMeasuredWidth() - dp32;

		for (int i = 0; i < 6; i++) {
			float diff = i * dp16 * 3;
			if (i == 0) {
				canvas.drawLine(0, sp12 + dp4 + diff, xChartLen, sp12 + dp4 + diff, originLinePaint);
			} else {
				canvas.drawLine(0, sp12 + dp4 + diff, xChartLen, sp12 + dp4 + diff, horizontalLinePaint);
			}
		}

		float chartScreenSize = previewRightBorder - previewLeftBorder;
		float zoom = xChartLen / chartScreenSize;
		fullChartSize = xChartLen * zoom;
		fullChartShift = previewLeftBorder * zoom;

		xSize = chart.xValues.length;
		long xMin = chart.xValues[0];
		long xMax = chart.xValues[xSize - 1];
		float xDiff = xMax - xMin;

		Float selectedPointX = null;
		if (selectedPoint != null) {
			selectedPointX =
				((chart.xValues[selectedPoint] - xMin) / xDiff) * fullChartSize - fullChartShift;
		}

		if (selectedPointX != null) {
			canvas.drawLine(selectedPointX, 0, selectedPointX, chartHeight, verticalLinePaint);
		}

		int startIndex = Math.round(xSize * fullChartShift / fullChartSize);
		int endIndex = Math.round(xSize * (fullChartShift + xChartLen) / fullChartSize);

		long yMin = Long.MAX_VALUE;
		long yMaxTarget = Long.MIN_VALUE;
		long yMaxLocalTarget = Long.MIN_VALUE;
		for (int l = chart.lines.length - 1; l >= 0; l--) {
			Line line = chart.lines[l];
			if (disabledLines.contains(line.id)) {
				continue;
			}
			if (line.minValue < yMin) {
				yMin = line.minValue;
			}
			if (line.maxValue > yMaxTarget) {
				yMaxTarget = line.maxValue;
			}

			for (int i = startIndex; i < endIndex; i++) {
				long value = line.values[i];
				if (value > yMaxLocalTarget) {
					yMaxLocalTarget = value;
				}
			}
		}
		if (yMin == Long.MAX_VALUE) {
			yMin = chart.yMinValue;
		}
		if (yMaxTarget == Long.MIN_VALUE) {
			if (yMax != -1) {
				yMaxTarget = yMax;
				yMaxLocalTarget = yMax;
			} else {
				yMaxTarget = chart.yMaxValue;
				yMaxLocalTarget = chart.yMaxValue;
			}
		}

		if (yMax == -1) {
			yMax = yMaxLocalTarget;
		}
		//boolean isAnimating = false;
		if (yMax != yMaxLocalTarget) {
			// TODO no time for ValueAnimator's ;(
			float animStep = chart.yMaxValue / 30f;
			if (yMax > yMaxLocalTarget) {
				yMax -= animStep;
			} else {
				yMax += animStep;
			}
			if (Math.abs(yMaxLocalTarget - yMax) < animStep) {
				yMax = yMaxLocalTarget;
			}
			if (yMax != yMaxLocalTarget) {
				//isAnimating = true;
				postInvalidate();
			}
		}

		float yDiffTarget = yMaxTarget - yMin;

		for (int l = chart.lines.length - 1; l >= 0; l--) {
			Line line = chart.lines[l];
			if (disabledLines.contains(line.id)) {
				continue;
			}
			linePaint.setColor(line.color);

			float[] pts = new float[xSize * 2];
			int j = 0;
			for (int i = 0; i < xSize; i++) {
				pts[j] = ((chart.xValues[i] - xMin) / xDiff) * fullChartSize - fullChartShift;
				j++;
				pts[j] = chartHeight * (1 - ((line.values[i]) / (float) yMax));
				j++;
			}
			canvas.drawLines(pts, linePaint);
			canvas.drawLines(pts, 2, xSize * 2 - 2, linePaint);
			if (selectedPointX != null) {
				float pointY = ((line.values[selectedPoint]) / (float) yMax) * -chartHeight + chartHeight;
				canvas.drawCircle(selectedPointX, pointY, dp4, whiteFillPaint);
				canvas.drawCircle(selectedPointX, pointY, dp4, linePaint);
			}
		}
		for (int i = 0; i < 6; i++) {
			float diff = i * dp16 * 3;
			canvas.drawText(
				String.valueOf((yMax / 5) * (5 - i)),
				0,
				sp12 + diff,
				chartLegendPaint
			);
		}

		if (selectedPointX != null) {
			int valuesCount = 0;
			float maxLineNameLen = 0;
			float maxLineValueLen = 0;
			for (int i = 0; i < chart.lines.length; i++) {
				Line line = chart.lines[i];
				if (disabledLines.contains(line.id)) {
					continue;
				}
				float len = selectedLineNamePaint.measureText(line.name);
				if (len > maxLineNameLen) {
					maxLineNameLen = len;
				}
				len = selectedLineValuePaint.measureText(String.valueOf(line.maxValue));
				if (len > maxLineValueLen) {
					maxLineValueLen = len;
				}
				len = selectedLineValuePaint.measureText(String.valueOf(line.minValue));
				if (len > maxLineValueLen) {
					maxLineValueLen = len;
				}
				valuesCount++;
			}
			float maxLineNameValueLen = Math.max(maxLineNameLen, maxLineValueLen);
			if (valuesCount > 0) {
				int linesCount = (int) Math.ceil(valuesCount / 2f);
				int multiplier = valuesCount > 1 ? 2 : 1;
				String selectedDateText =
					SELECTED_DATE_FORMAT.format(new Date(chart.xValues[selectedPoint]));
				float selectedDateTextLen = selectedDatePaint.measureText(selectedDateText);
				float selectedSizeHalfX = Math.max(
					selectedDateTextLen + dp8 * 2,
					maxLineNameValueLen * multiplier + dp8 * (multiplier + 1)
				) / 2f;
				float selectedSizeY = dp4 + sp12 + linesCount * (dp8 + sp16 + dp2 + sp12) + dp4;

				drawSelectedView(canvas, selectedSizeHalfX, selectedSizeY, dp4, selectedPointX);

				canvas.drawText(
					selectedDateText,
					selectedPointX - selectedSizeHalfX + dp8,
					dp4 + sp12,
					selectedDatePaint
				);
				int n = 0;
				for (int i = 0; i < chart.lines.length; i++) {
					Line line = chart.lines[i];
					if (disabledLines.contains(line.id)) {
						continue;
					}
					selectedLineValuePaint.setColor(line.color);
					selectedLineNamePaint.setColor(line.color);
					float x;
					if (n % 2 == 0) {
						x = selectedPointX - selectedSizeHalfX + dp8;
					} else {
						x = selectedPointX + dp4;
					}
					n++;
					int currentLine = (int) Math.ceil(n / 2f);
					canvas.drawText(
						String.valueOf(line.values[selectedPoint]),
						x,
						dp4 + sp12 + currentLine * (dp8 + sp16) + (currentLine - 1) * (dp2 + sp12),
						selectedLineValuePaint
					);
					//noinspection IntegerDivisionInFloatingPointContext
					canvas.drawText(
						line.name,
						x,
						dp4 + sp12 + currentLine * (dp8 + sp16 + dp2 + sp12),
						selectedLineNamePaint
					);
				}
			}
		}

		canvas.translate(0, dp16 * 16.5f);
		// i = (fullChartSize + dp8) / (dateMaxLen + dp8)
		// dp8 = (fullChartSize - i*dateMaxLen) / (i - 1)
		float dateCountMax = (fullChartSize + dp16) / (dateMaxLen + dp16);
		int dateCount = (int) ((xChartLen + dp16) / (dateMaxLen + dp16));
		while (dateCountMax > 2 * dateCount - 1) {
			dateCount = 2 * dateCount - 1;
		}
		float shift = (fullChartSize - dateCount * dateMaxLen) / (dateCount - 1);
		int indStep = xSize / dateCount;
		for (int i = 0; i < dateCount; i++) {
			long date = i == dateCount - 1 ? chart.xValues[xSize - 1] : chart.xValues[i * indStep];
			canvas.drawText(
				DATE_FORMAT.format(new Date(date)),
				i * (dateMaxLen + shift) - fullChartShift,
				sp12,
				chartLegendPaint
			);
		}
		float percentOf = dateCountMax / (2 * dateCount - 1);
		float animationThreshold = .85f;
		if (percentOf > animationThreshold) {
			float alpha = (percentOf - animationThreshold) / (1 - animationThreshold);
			chartLegendPaint.setAlpha((int) (255 * alpha));
			dateCount = 2 * dateCount - 1;
			shift = (fullChartSize - dateCount * dateMaxLen) / (dateCount - 1);
			indStep = xSize / dateCount;
			for (int i = 0; i < dateCount; i++) {
				if (i % 2 == 0) {
					continue;
				}
				long date = chart.xValues[i * indStep];
				canvas.drawText(
					DATE_FORMAT.format(new Date(date)),
					i * (dateMaxLen + shift) - fullChartShift,
					sp12,
					chartLegendPaint
				);
			}
		}
		chartLegendPaint.setAlpha(255);

		canvas.translate(0, dp16 * 2.5f);
		for (int l = chart.lines.length - 1; l >= 0; l--) {
			Line line = chart.lines[l];
			if (disabledLines.contains(line.id)) {
				continue;
			}
			linePreviewPaint.setColor(line.color);

			float[] pts = new float[xSize * 2];
			int j = 0;
			for (int i = 0; i < xSize; i++) {
				pts[j] = ((chart.xValues[i] - xMin) / xDiff) * xChartLen;
				j++;
				pts[j] = (dp32 - dp2) * (1 - ((line.values[i] - yMin) / yDiffTarget));
				j++;
			}
			canvas.drawLines(pts, linePreviewPaint);
			canvas.drawLines(pts, 2, xSize * 2 - 2, linePreviewPaint);
		}
		if (previewLeftBorder == previewRightBorder) {
			canvas.drawRect(0, 0, xChartLen, dp32, previewOutsidePaint);
		} else {
			canvas.drawRect(0, 0, previewLeftBorder, dp32, previewOutsidePaint);

			canvas.drawRect(previewLeftBorder, 0, previewLeftBorder + dp4, dp32, previewFramePaint);
			canvas.drawRect(previewLeftBorder, 0, previewRightBorder, dp1, previewFramePaint);
			canvas.drawRect(previewLeftBorder, dp32 - dp1, previewRightBorder, dp32, previewFramePaint);
			canvas.drawRect(previewRightBorder - dp4, 0, previewRightBorder, dp32, previewFramePaint);

			canvas.drawRect(previewRightBorder, 0, xChartLen, dp32, previewOutsidePaint);
		}

		if (DEBUG) {
			canvas.restore();
			// chart
			canvas.drawRect(0, dp16 * 3, getMeasuredWidth(), dp16 * 19, debugPaint);
			// preview
			canvas.drawRect(previewLeftBorder, dp16 * 21, previewRightBorder + dp32, dp16 * 25, debugPaint);
			// previewLeft
			canvas.drawRect(previewLeftBorder, dp16 * 21, previewLeftBorder + dp32, dp16 * 25, debugPaint);
			// previewRight
			canvas.drawRect(previewRightBorder, dp16 * 21, previewRightBorder + dp32, dp16 * 25, debugPaint);
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int desiredWidth = (int) PixelUtil.dp(getResources(), 200);
		int desiredHeight = (int) (dp16 * 25);

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width;
		int height;

		if (widthMode == MeasureSpec.EXACTLY) {
			width = widthSize;
		} else if (widthMode == MeasureSpec.AT_MOST) {
			width = Math.min(desiredWidth, widthSize);
		} else {
			width = desiredWidth;
		}

		if (heightMode == MeasureSpec.EXACTLY) {
			height = heightSize;
		} else if (heightMode == MeasureSpec.AT_MOST) {
			height = Math.min(desiredHeight, heightSize);
		} else {
			height = desiredHeight;
		}

		setMeasuredDimension(width, height);

		if (previewRightBorderRatio == -1) {
			previewRightBorderRatio = 1f;
		}
		if (previewLeftBorderRatio == -1) {
			previewLeftBorderRatio = 0.8f;
		}
		previewRightBorder = (width - dp32) * previewRightBorderRatio;
		previewLeftBorder = (width - dp32) * previewLeftBorderRatio;
		previewMinSize = (width - dp32) * 0.2f;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			state = bundle.getParcelable(STATE_SUPER);
			if (bundle.containsKey(STATE_SELECTED_POINT)) {
				selectedPoint = bundle.getInt(STATE_SELECTED_POINT);
			}
			previewLeftBorderRatio = bundle.getFloat(STATE_PREVIEW_LEFT_BORDER);
			previewRightBorderRatio = bundle.getFloat(STATE_PREVIEW_RIGHT_BORDER);
			List<String> lines = bundle.getStringArrayList(STATE_DISABLED_LINES);
			if (lines != null) {
				disabledLines.addAll(lines);
			}
		}
		super.onRestoreInstanceState(state);
	}

	@Nullable
	@Override
	protected Parcelable onSaveInstanceState() {
		Bundle bundle = new Bundle();
		bundle.putParcelable(STATE_SUPER, super.onSaveInstanceState());
		if (selectedPoint != null) {
			bundle.putInt(STATE_SELECTED_POINT, selectedPoint);
		}
		bundle.putFloat(STATE_PREVIEW_LEFT_BORDER, previewLeftBorderRatio);
		bundle.putFloat(STATE_PREVIEW_RIGHT_BORDER, previewRightBorderRatio);
		bundle.putStringArrayList(STATE_DISABLED_LINES, new ArrayList<>(disabledLines));
		return bundle;
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				isChartTouch = hitChart(event);
				isPreviewTouch = hitPreview(event);
				isPreviewLeftBorderTouch = hitPreviewLeftBorder(event);
				isPreviewRightBorderTouch = hitPreviewRightBorder(event);
				if (!isChartTouch && !isPreviewTouch) {
					return false;
				}
				if (isPreviewTouch) {
					previousTouchEventX = event.getX() - dp16;
				}
			}

			if (isChartTouch) {
				float selectedDate = event.getX() - dp16;
				if (selectedDate < dp16 * 3) {
					selectedDate = dp16 * 3;
				} else if (selectedDate > getMeasuredWidth() - dp16 * 3) {
					selectedDate = getMeasuredWidth() - dp16 * 3;
				}
				float selectedDateNorm = (fullChartShift + selectedDate) / fullChartSize;
				selectedPoint = Math.round(xSize * selectedDateNorm);
			} else if (isPreviewLeftBorderTouch) {
				float newLeftBorder = event.getX() - dp16;
				if (previewRightBorder - newLeftBorder < previewMinSize) {
					newLeftBorder = previewRightBorder - previewMinSize;
				}
				previewLeftBorder = newLeftBorder;
				if (previewLeftBorder < 0) {
					previewLeftBorder = 0;
				} else if (previewLeftBorder > getMeasuredWidth() - dp16 * 2 - previewMinSize) {
					previewLeftBorder = getMeasuredWidth() - dp16 * 2 - previewMinSize;
				}
			} else if (isPreviewRightBorderTouch) {
				float newRightBorder = event.getX() - dp16;
				if (newRightBorder - previewLeftBorder < previewMinSize) {
					newRightBorder = previewLeftBorder + previewMinSize;
				}
				previewRightBorder = newRightBorder;
				if (previewRightBorder < previewMinSize) {
					previewRightBorder = previewMinSize;
				} else if (previewRightBorder > getMeasuredWidth() - dp16 * 2) {
					previewRightBorder = getMeasuredWidth() - dp16 * 2;
				}
			} else if (isPreviewTouch) {
				float diff = previousTouchEventX - (event.getX() - dp16);
				previewLeftBorder -= diff;
				previewRightBorder -= diff;
				previousTouchEventX = event.getX() - dp16;
				float size = previewRightBorder - previewLeftBorder;
				if (previewLeftBorder < 0) {
					previewLeftBorder = 0;
					previewRightBorder = size;
				} else if (previewRightBorder > getMeasuredWidth() - dp16 * 2) {
					previewLeftBorder = getMeasuredWidth() - dp16 * 2 - size;
					previewRightBorder = getMeasuredWidth() - dp16 * 2;
				}
			}
			previewLeftBorderRatio = previewLeftBorder / (getMeasuredWidth() - dp32);
			previewRightBorderRatio = previewRightBorder / (getMeasuredWidth() - dp32);

			invalidate();
			getParent().requestDisallowInterceptTouchEvent(true);
			return true;
		}
		getParent().requestDisallowInterceptTouchEvent(false);
		return false;
	}

	public void setChart(@Nullable Chart chart) {
		this.chart = chart;
		invalidate();
	}

	public void setLineEnabled(String id, boolean enabled) {
		if (enabled) {
			disabledLines.remove(id);
		} else {
			disabledLines.add(id);
		}
		invalidate();
	}

	private boolean hitChart(MotionEvent event) {
		return event.getY() > dp16 * 3 && event.getY() < dp16 * 19;
	}

	private boolean hitPreview(MotionEvent event) {
		return event.getY() > dp16 * 21 && event.getY() < dp16 * 25
			&& event.getX() > previewLeftBorder && event.getX() < previewRightBorder + dp32;
	}

	private boolean hitPreviewLeftBorder(MotionEvent event) {
		return event.getY() > dp16 * 21 && event.getY() < dp16 * 25
			&& event.getX() > previewLeftBorder && event.getX() < previewLeftBorder + dp32;
	}

	private boolean hitPreviewRightBorder(MotionEvent event) {
		return event.getY() > dp16 * 21 && event.getY() < dp16 * 25
			&& event.getX() > previewRightBorder && event.getX() < previewRightBorder + dp32;
	}

	private void drawSelectedView(
		Canvas canvas,
		float selectedSizeHalfX,
		float selectedSizeY,
		float radius,
		float selectedPointX
	) {
		RectF oval = new RectF(
			selectedPointX - dp32,
			0,
			selectedPointX - dp32 + radius * 2,
			radius * 2
		);
		canvas.drawArc(oval, 180, 90, false, selectedBorderPaint);
		canvas.drawArc(oval, 180, 90, true, selectedBackgroundPaint);
		oval.set(
			selectedPointX + selectedSizeHalfX - radius * 2,
			0,
			selectedPointX + selectedSizeHalfX,
			radius * 2
		);
		canvas.drawArc(oval, 270, 90, false, selectedBorderPaint);
		canvas.drawArc(oval, 270, 90, true, selectedBackgroundPaint);
		oval.set(
			selectedPointX - selectedSizeHalfX,
			selectedSizeY - radius * 2,
			selectedPointX - selectedSizeHalfX + radius * 2,
			selectedSizeY
		);
		canvas.drawArc(oval, 90, 90, false, selectedBorderPaint);
		canvas.drawArc(oval, 90, 90, true, selectedBackgroundPaint);
		oval.set(
			selectedPointX + selectedSizeHalfX - radius * 2,
			selectedSizeY - radius * 2,
			selectedPointX + selectedSizeHalfX,
			selectedSizeY
		);
		canvas.drawArc(oval, 0, 90, false, selectedBorderPaint);
		canvas.drawArc(oval, 0, 90, true, selectedBackgroundPaint);
		canvas.drawLine(
			selectedPointX - selectedSizeHalfX,
			radius,
			selectedPointX - selectedSizeHalfX,
			selectedSizeY - radius,
			selectedBorderPaint
		);
		canvas.drawLine(
			selectedPointX - selectedSizeHalfX + radius,
			0,
			selectedPointX + selectedSizeHalfX - radius,
			0,
			selectedBorderPaint
		);
		canvas.drawLine(
			selectedPointX + selectedSizeHalfX,
			radius,
			selectedPointX + selectedSizeHalfX,
			selectedSizeY - radius,
			selectedBorderPaint
		);
		canvas.drawLine(
			selectedPointX - selectedSizeHalfX + radius,
			selectedSizeY,
			selectedPointX + selectedSizeHalfX - radius,
			selectedSizeY,
			selectedBorderPaint
		);
		canvas.drawRect(
			selectedPointX - selectedSizeHalfX,
			radius,
			selectedPointX + selectedSizeHalfX,
			selectedSizeY - radius,
			selectedBackgroundPaint
		);
		canvas.drawRect(
			selectedPointX - selectedSizeHalfX + radius,
			0,
			selectedPointX + selectedSizeHalfX - radius,
			selectedSizeY,
			selectedBackgroundPaint
		);
	}
}